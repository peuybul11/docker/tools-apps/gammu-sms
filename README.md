# Gammu SMS

Gammu SMSD is a service to mass send and receive SMS messages. Both these are built on top of Gammu library, which provides abstraction layer to work with different cell phones from different vendors (including Nokia, Sony-Ericsson, Motorola, Samsung, Siemens, Huawei and others). And 3G/4G Dongle.

To get started 1 Plug in your 3G/4G Dongle into your Laptop/PC/Server or Raspberry PI. Check if your dongle is found by executing: lsusb

Once you have found create a Docker MySQL container and a database

Execute:
```
docker run --name sms-sender --restart always -i --device=/dev/ttyUSB1 --publish-all -p 8080:80 -d valterseu/gammu-sms
```

The `/dev/ttyUSB1` Can be different from the above-mentioned example, based on your system and to which USB port you have your device connected.

Once the Docker container is Running change the configuration in the Docker container `/etc/gammu-smsdrc`

```
port = /dev/ttyUSB1
connection = AT
speed = 9600

How often to reset the device if it is stuck
ResetFrequency = 11
A hard reset, so that 3G Doungle doesn't get stuck
HardResetFrequency = 20
How often to check for incoming TEXT Messages
ReceiveFrequency = 10
How often to check for GSM connection ( Suggested every 30 minutes )
StatusFrequency = 30
How often to check outgoing SMS if there are none stuck? ( Suggested every 15 minutes )
CommTimeout = 5
How long to wait for a mobile carrier to answer if an SMS is sent? By default it is 30 seconds
SendTimeout = 3
If set to 1 then Doungle will go into standby if no activity ( Suggested to put leave as is or set 0 )
LoopSleep = 0
How long does to wait for a large text SMS to be sent Suggested 6 seconds
MultipartTimeout = 6
Reject incoming phone calls to the SIM
HangupCalls = 1
Check if the SIM card wants a pin
CheckSecurity = 1
How many times to retry resending an SMS if something goes wrong?
MaxRetries = 1
After how many seconds to retry sending failed SMS Messages?
RetryTimeout = 5

[smsd]

MySQL Connection
service = sql
driver = native_mysql
host = MySQL_HOST
user = MySQL_USER
password = MySQL_USER_PASSWORD
database = MySQL_DB_NAME

SMSD configuration, see gammu-smsdrc(5)

OutboxFormat = unicode
TransmitFormat = unicode
PIN = 0000
RetryTimeout = 5
MultipartTimeout = 5
ResetFrequency = 30
HardResetFrequency = 60
DeliveryReport = sms
PhoneID = first
logfile = /var/log/sms_log
debuglevel = 255
StatusFrequency = 0
CommTimeout = 7
SendTimeout = 10
HangupCalls = 1
LoopSleep = 0
CheckSecurity = 0
```

To send SMS using web post execute

Example:
`http://<127.0.0.1:8080/index.php?rec=37122222222&text=Text%20Message`
Or in Docker terminal:
`gammu-smsd-inject --config=/etc/gammu-smsdrc TEXT 371222222 -unicode -textutf8 Message`